from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.validators import UniqueValidator
from .models import Order
from rest_framework import permissions


#  `````````````  ORDER `` ``````````
class OrdersSerializer(serializers.ModelSerializer):
    # products_name = serializers.CharField(source='products')
    tel = serializers.CharField(required=True, max_length=16)
    email = serializers.CharField(max_length=255)

    class Meta:
        model = Order
        fields = ('products', 'tel', 'email', 'messenge', 'created', 'updates')
