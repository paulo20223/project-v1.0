from rest_framework import permissions
from .models import Order


#  ````````````` LIST EDIT ORDERS `` ``````````
class ListEditPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return request.user.type_user == "2"
