from rest_framework.response import Response
from .serializer import *
from rest_framework import generics
from rest_framework.permissions import *
from .permissions import *
from count.models import Product

# Create orders
class CreateOrders(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrdersSerializer
    permission_classes = (IsAuthenticated, )
    def perform_create(self, serializer):
        product_create = Product.objects.get(id=int(self.request.data['products'])).user_create
        return serializer.save(owner=product_create)


class ListOrders(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrdersSerializer
    permission_classes = (IsAuthenticated, ListEditPermission)
    def get_queryset(self, *args, **kwargs):
        return Order.objects.filter(owner=self.request.user.id)



class OrdersEdit(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrdersSerializer
    permission_classes = (IsAuthenticated, ListEditPermission)
    def get_queryset(self, *args, **kwargs):
        return Order.objects.filter(owner=self.request.user.id)