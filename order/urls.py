from django.urls import include, path, re_path
from rest_framework import routers
from . import views

urlpatterns = [
    path('edit/<int:pk>', views.OrdersEdit.as_view()),
    path('add/', views.CreateOrders.as_view()),
    path('', views.ListOrders.as_view()),
]