from count.models import *
from django.db import models
from django.db.models.signals import post_save
from django import forms
from count.models import Product
from users.models import MESSENGE


class Order(models.Model):
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    tel = models.CharField('Контактный телефон', max_length=15)
    email = models.EmailField('Контактный email', max_length=255)
    messenge = models.CharField('Мессенджер для связи', choices=MESSENGE, default='v', max_length=1)
    owner = models.IntegerField('Продавец')
    created = models.DateField(auto_now_add=True, auto_now=False)
    updates = models.DateField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return "Заявка {0}".format(self.id)

    class Meta:
        verbose_name = "Заявка"
        verbose_name_plural = "Заявки"

