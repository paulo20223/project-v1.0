Проект Площадка для объявлений.

Count - Продукты и Категории Продуктов. 
Users - Кастом библиотеки django-rest-auth
Order - Объявления

Старт проекта 

!!! 
Необходимо находиться в папке проекта 

!!Виртуальное окружение!!
Установить
pip install virtualenv

Создать в проекте
virtualenv env

Запустить
source bin/activate

Установка библиотек
pip install -r r.txt

Миграция
python manage.py makemigrations

python manage.py migrate

Запуск сервера
python manage.py runserver