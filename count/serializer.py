from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.validators import UniqueValidator
from .models import Product, Category
from rest_framework import permissions
from .permissions import IsUsers


#  `````````````  PRODUCTS `` ``````````
class ProductSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'price', 'user_create', 'updates')


#  `````````````  PRODUCTS DETAILS ````````````
class ProductDetaisSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=True)
    description = serializers.CharField(required=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'status', 'category', 'price', 'user_create', 'updates')



#  ````````````` CREATE PRODUCTS `` ``````````
class CreateProductSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=True)
    description = serializers.CharField(required=True)

    class Meta:
       model = Product
       fields = ('id', 'name', 'description', 'category', 'price')


#  `````````````  CATEGORIES `` ``````````
class CategoriesSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    meta = serializers.CharField(required=False)

    class Meta:
        model = Category
        fields = ('name', 'meta')




