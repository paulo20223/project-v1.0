from django.urls import include, path, re_path
from rest_framework import routers
from count import views

urlpatterns = [
    path('products/', views.ProductAllList.as_view()),
    path('products/<int:pk>/', views.ProductDetail.as_view()),
    path('products/edit/<int:pk>/', views.ProductEdit.as_view()),
    path('products/add/', views.CreateProduct.as_view()),
    path('categorie/', views.CategoieAllList.as_view()),
    path('categorie/<int:pk>/', views.CategoieDetail.as_view()),
    path('categorie/add/', views.CreateCategoie.as_view()),

]