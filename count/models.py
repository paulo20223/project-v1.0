from django.db import models
from django.utils.html import format_html
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

STATUS = (('1','Заявка принята'),
          ('2', 'Опубликовано'),
          ('3','Заблокированно'),
          ('4', 'Продано'),
          ('5', 'Устарело'),)

#Category product
class Category(models.Model):
    name = models.CharField("Имя", max_length=300)
    meta = models.TextField('Мета описание', max_length=1000, help_text="Для продвижения", null=True, default=None, blank=True)

    def __str__(self):
        return '{0}'.format(self.name)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

#Product
class Product(models.Model):
    name = models.CharField("Название товара", max_length=255)
    status = models.CharField("Статус", choices=STATUS, default=1, max_length=1)
    description = models.TextField('Описние товара', max_length=5000)
    category = models.ManyToManyField(Category, verbose_name="Категории")
    meta = models.TextField('Мета', max_length=2000, null=True)
    price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    user_create = models.IntegerField('ID создателя', default=0)
    image = models.ImageField('Изображение товара', max_length=5000)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updates = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{0}'.format(self.name)

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"

#Galery product
class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, name='Продукт', null=True)
    image = models.ImageField(upload_to="image/", default="/default.jpg", help_text='Выберите картинку')

    def __str__(self):
        return '{0} ({1})'.format(self.id, self.image)

    def image_product(self):
        return format_html('<img src="%s" width="150">' % (self.image.url))

    class Meta:
        verbose_name = "Фотография"
        verbose_name_plural = "Фотографии"