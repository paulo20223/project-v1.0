from rest_framework.response import Response
from .serializer import *
from rest_framework import generics
from rest_framework.permissions import *
from .permissions import *
from .pagnation import LargeResultsSetPagination, StandardResultsSetPagination

#  `````````````  PRODUCTS `` ``````````
class ProductAllList(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = StandardResultsSetPagination
    paginate_by = 10



class CreateProduct(generics.CreateAPIView):
    queryset = Product.objects.filter(status=2)
    serializer_class = CreateProductSerializer
    permission_classes = (CreateEditPermission, )
    def perform_create(self, serializer):
        serializer.save(user_create=self.request.user.id)
        serializer.save()

class ProductDetail(generics.ListAPIView):
    queryset = Product.objects.filter(status=2)
    serializer_class = ProductSerializer
    def get_queryset(self, *args, **kwargs):
        id = self.kwargs['pk']
        return Product.objects.filter(id=id)


class ProductEdit(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated, EditPremission)


#  `````````````  Categoies `` ``````````
class CategoieAllList(generics.GenericAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoriesSerializer


class CreateCategoie(generics.CreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoriesSerializer
    permission_classes = (IsAdminUser, )
    def perform_create(self, serializer):
        if self.request.data['meta'] == None:
            serializer.save(meta=self.request.data['name'])
        else:
            serializer.save()


class CategoieDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoriesSerializer
    permission_classes = (IsAdminUser, )
