from rest_framework import permissions
from .models import Product

#  ````````````` PRODUCTS `` ``````````
class IsUsers(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True


# Permission create products
#  ````````````` CREATE EDIT `` ``````````
class CreateEditPermission(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        if request.user.type_user == "2":
            return True
        else:
            return False



# Permission edit products
class EditPremission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        user_create = Product.objects.get(id=request.parser_context["kwargs"]["pk"]).user_create
        return user_create == request.user.id

