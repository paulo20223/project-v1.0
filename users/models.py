from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.contrib.auth.models import User

MESSENGE = (('v', 'Viber',),('t', 'Telegram'),('w', 'Whatsapp'))
USER_TYPE = ((1, 'Покупатель'), (2, 'Продавец'))


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, tel, messenge, type_user,  password=None):
        user = self.model(
            email=self.normalize_email(email),
            tel=tel,
            messenge=messenge,
            type_user=type_user,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, tel, messenge, type_user, password):
        user = self.create_user(
            email,
            password=password,
            tel=tel,
            messenge=messenge,
            type_user=type_user,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, tel, messenge, type_user, password):
        user = self.create_user(
            email,
            password=password,
            tel=tel,
            messenge=messenge,
            type_user=type_user,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user

class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(('Email'), unique=True)
    tel = models.CharField(max_length=16)
    messenge = models.CharField(choices=MESSENGE, max_length=1)
    type_user = models.CharField(choices=USER_TYPE, max_length=1)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['tel', 'messenge', 'tel', 'type_user']

    objects = UserManager()

    def __str__(self):  # __unicode__ on Python 2
        return self.email