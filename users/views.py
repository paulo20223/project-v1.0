from rest_framework import generics
from .models import CustomUser
from . import serializers
from rest_framework.permissions import *
from rest_auth.registration.views import RegisterView


class UserListView(generics.ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated, )

class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated, )