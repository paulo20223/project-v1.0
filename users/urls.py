from django.urls import include, path, re_path

from . import views

urlpatterns = [
    path('' , views.UserListView.as_view()),
    path('<int:pk>/' , views.UserDetailView.as_view()),
    path('', include('rest_auth.urls')),
    path('registration/', include('rest_auth.registration.urls')),
]