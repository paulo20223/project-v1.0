from rest_framework import serializers
from .models import MESSENGE, USER_TYPE, CustomUser
from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import LoginSerializer, UserDetailsSerializer, UserModel
from rest_auth import serializers as auth_serializers
from allauth.account.adapter import get_adapter


class MyUserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('pk','email', 'tel', 'messenge', 'type_user')
        read_only_fields = ('email',)

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('pk', 'tel', 'email', 'type_user', 'messenge')

class MyLoginSerializer(LoginSerializer):
    username = None


class MyRegisterSerializer(RegisterSerializer):
    username = None
    tel = serializers.CharField(max_length=16, required=True)
    type_user = serializers.ChoiceField(choices=USER_TYPE, required=True)
    messenge = serializers.ChoiceField(choices=MESSENGE, required=True)

    def get_cleaned_data(self):
        super(MyRegisterSerializer, self).get_cleaned_data()
        return {
            'username': None,
            'tel': self.validated_data.get('tel', ''),
            'type_user': self.validated_data.get('type_user', ''),
            'password1': self.validated_data.get('password1', ''),
            'password2': self.validated_data.get('password2', ''),
            'email': self.validated_data.get('email', ''),
        }

    def custom_signup(self, request, user):
        user.username = None
        user.tel = self.validated_data.get('tel', '')
        user.type_user = self.validated_data.get('type_user', '')
        user.messenge = self.validated_data.get('messenge', '')
        user.save(update_fields=['tel', 'type_user', 'messenge'])